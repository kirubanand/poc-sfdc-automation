@SDFC @Assets
Feature: Assets
  In order test SFDC Assets module
  As a tester
  And I should verify as per client requirement

  @create-Assets
  Scenario: create assets
    Given I should navigate to salesforce Home page
    Then I should see salesforce home page succesfully
    When I click on assets tab from all tabs section
    Then I should see following:
      """
      Asset
      Home
      """
    And I should see assets page succesfully
    When I follow on "New" button
    Then I should see following:
      """
      Asset Edit
      New Asset
      """
    When I fill in "Asset Name" with random "smoke-test-Asset-1-"
    And I fill in the following:
      | field   | value                  |
      | Account | smoke-test-account-123 |
      | Contact | smoke-test-contact-123 |
    And I click on "Save" button from top row