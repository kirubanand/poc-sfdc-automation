@SDFC @opportunity
Feature:opportunity
  In order test SFDC Opportunity module
  As a tester
  And I should verify as per client requirement

  @create-opportunity
  Scenario Outline: create opportunity
    Given I should navigate to salesforce Home page
    Then I should see salesforce home page succesfully
    When I click on Opportunity tab from all tabs section
    Then I should see Opportunity page succesfully
    And I should see following:
      """
      Opportunities
      Home
      """
    When I follow on "New" button
    Then I should see following:
      """
      Opportunity Edit
      New Opportunity
      """
    When I fill in the following:
      | field           | value                  |
      | Account Name    | smoke-test-account-123 |
      | Close Date      | 12/21/2018             |
      | Tracking Number | 001                    |
      | Order Number    | 001                    |
    And I select the following:
      | field                        | value         |
      | Stage                        | Qualification |
      | Type                         | Web           |
      | Type                         | Web           |
      | Delivery/Installation Status | In progress   |
    And I fill in "Opportunity Name" with random "<Opportunity-Name>"
    And I click on "Save" button from top row

    Examples:
      | Opportunity-Name          |
      | smoke-test-Opportunity-1- |