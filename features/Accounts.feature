@SDFC @accounts
Feature:Accounts
  In order test SFDC Accounts module
  As a tester
  And I should verify as per client requirement

  @create-accounts
  Scenario: Create account
    Given I should navigate to salesforce Home page
    Then I should see salesforce home page succesfully
    When I click on Accounts tab from all tabs section
    Then I should see following:
      """
      Accounts
      Home
      """
    And I should see account page succesfully
    When I follow on "New" button
    Then I should see following:
      """
      Account Edit
      New Account
      """
    When I fill in "Account Name" with random "smoke-test-accounts-1-"
    And I click on "Save" button from top row