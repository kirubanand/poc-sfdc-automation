@SDFC @Leads
Feature:Leads
  In order test SFDC Leads module
  As a tester
  And I should verify as per client requirement

  @create-leads
  Scenario: Create Leads
    Given I should navigate to salesforce Home page
    Then I should see salesforce home page succesfully
    When I click on Leads tab from all tabs section
    Then I should see following:
      """
      Leads
      Home
      """
    And I should see leads page succesfully
    When I follow on "New" button
    Then I should see following:
      """
      Lead Edit
      New Lead
      """
    When I fill in "Last Name" with random "smoke-test-leads-1-"
    And I fill in the following:
      | field   | value                    |
      | Company | Positive Edge Technology |
    And I click on "Save" button from top row