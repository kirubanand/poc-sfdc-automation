When(/^I fill in the following:$/) do |table|
  # table is a Cucumber::MultilineArgument::DataTable
  table.hashes.each do |row|
    fill_in(row["field"], with: row["value"], match: :prefer_exact)
    end
end

When(/^I select the following:$/) do |table|
  # table is a Cucumber::MultilineArgument::DataTable
  table.hashes.each do |row|
    select(row["value"], :from => row["field"], match: :prefer_exact)
  end
end

When(/^I click on Accounts tab from all tabs section$/) do
  find(:xpath,".//*[@id='AllTab_Tab']/a/img").click
  sleep 20
  find(:xpath,".//a[@class='listRelatedObject accountBlock title']").click
  sleep 20
end

Then(/^I should see create Opportunity page succesfully$/) do
  $pageTitle = page.title
  puts "current page title = #{$pageTitle}"
  if $pageTitle == "Opportunity Edit: New Opportunity ~ Salesforce - Performance Edition"
    puts "succesfully visited home page"
  else
    writeFailure "Not visited home page successfully"
  end
end

Then(/^I should see following:$/) do |string|
  if page.has_content?(string)
    puts "string is visible"
  else
    writeFailure "Failed to see following string"
  end
end

Given(/^I should navigate to salesforce Home page$/) do
  value = getCredentialInfo
  visit get_url
  fill_in('username', :with => value["Username"])
  fill_in('password', :with => value["Password"])
  find(:id,"Login").click
  sleep 100
end

Then(/^I should see salesforce home page succesfully$/) do
  $pageTitle = page.title
  puts "current page title = #{$pageTitle}"
  if $pageTitle == "Salesforce - Performance Edition"
    puts "succesfully visited home page"
  else
    writeFailure "Not visited home page successfully"
  end
end

When(/^I click on Opportunity tab from all tabs section$/) do
  find(:xpath,".//*[@id='AllTab_Tab']/a/img").click
  sleep 20
  find(:xpath,".//a[@class='listRelatedObject opportunityBlock title']").click
  sleep 20
end

When(/^I click on Contacts tab from all tabs section$/) do
  find(:xpath,".//*[@id='AllTab_Tab']/a/img").click
  sleep 20
  find(:xpath,".//a[@class='listRelatedObject contactBlock title']").click
  sleep 20
end

When(/^I click on Leads tab from all tabs section$/) do
  find(:xpath,"//*[@id='AllTab_Tab']/a/img").click
  sleep 20
  find(:xpath,"//a[@class='listRelatedObject leadBlock title']").click
  sleep 20
end

Then(/^I should see Opportunity page succesfully$/) do
  $pageTitle = page.title
  puts "current page title = #{$pageTitle}"
  if $pageTitle == "Opportunities: Home ~ Salesforce - Performance Edition"
    puts "succesfully visited home page"
  else
    writeFailure "Not visited home page successfully"
  end
end

#code to click on all tabs section and click on products tab
When(/^I click on products tab from all tabs section$/) do
  find(:xpath,".//*[@id='AllTab_Tab']/a/img").click
  sleep 20
  find(:xpath,".//a[@class='listRelatedObject productBlock title']").click
  sleep 20
end

When(/^I click on assets tab from all tabs section$/) do
  find(:xpath,"//*[@id='AllTab_Tab']/a/img").click
  sleep 20
  find(:xpath,"//a[@class='listRelatedObject assetBlock title']").click
  sleep 20
end

Then(/^I should see leads page succesfully$/) do
  $pageTitle = page.title
  puts "current page title = #{$pageTitle}"
  if $pageTitle == "Leads: Home ~ Salesforce - Performance Edition"
    puts "succesfully visited home page"
  else
    writeFailure "Not visited home page successfully"
  end
end

Then(/^I should see assets page succesfully$/) do
  $pageTitle = page.title
  puts "current page title = #{$pageTitle}"
  if $pageTitle == "Assets: Home ~ Salesforce - Performance Edition"
    puts "succesfully visited home page"
  else
    writeFailure "Not visited home page successfully"
  end
end


Then(/^I should see contacts page succesfully$/) do
  $pageTitle = page.title
  puts "current page title = #{$pageTitle}"
  if $pageTitle == "Contacts: Home ~ Salesforce - Performance Edition"
    puts "succesfully visited Contacts page"
  else
    writeFailure "Not visited Contacts page successfully"
  end
end

Then(/^I should see account page succesfully$/) do
  $pageTitle = page.title
  puts "current page title = #{$pageTitle}"
  if $pageTitle == "Accounts: Home ~ Salesforce - Performance Edition"
    puts "succesfully visited Accounts page"
  else
    writeFailure "Not visited Accounts page successfully"
  end
end

When(/^I fill in "([^"]*)" with random "([^"]*)"$/) do |field_name, value|
  randomno=('0000'..'9999').to_a.sample
  randomvalue = value + randomno
  puts randomvalue
  fill_in(field_name, with: randomvalue, match: :prefer_exact)
  sleep 5
end

When(/^I click on "([^"]*)" check box field$/) do |field_label_name|
  check field_label_name
  sleep 5
end

When(/^I fill in "([^"]*)" with "([^"]*)"$/) do |field_name, value|
  fill_in(field_name, with: value, match: :prefer_exact)
  sleep 5
end

Then(/^I select "([^"]*)" from "([^"]*)" field$/) do |value, locator|
  if page.has_select? locator
    select(value, :from => locator, match: :prefer_exact)
  else
    writeFailure " #{locator} is not visible"
  end
end

#step definition to verify the content in a web page that specified in an feature step
Then(/^I should see content "([^"]*)"$/) do |content|
  sleep 5
  if page.has_content? content
    puts "#{content} content is present"
  else
    writeFailure "#{content} content is not present"
  end
end



Then(/^I should not see "([^"]*)" field$/) do |value|
  if page.has_field?(value)
    writeFailure "#{value} field is visible with editable permission"
  elsif page.has_content?(value)
    writeFailure "#{value} field is visible with readable permission"
  else
    puts "#{value} field is not visible"
  end
end

When(/^I click on all tabs view button$/) do
  find(:xpath,"//img[@title='All Tabs']").click
  sleep 5
end

Given(/^I should navigate to Home tab in saleforce application$/) do
  step 'I enter the credentials to the Zenith application'
  sleep 15
end

#step definition to click on bottom row button
When(/^I click on "([^"]*)" button from bottom row$/) do |val|
  within(:id,'bottomButtonRow') do
    if find(:button, val).visible?
      click_on val
      sleep 5
    end
  end
end

#step definition to verify the link in a web page that specified in an feature step
Then(/^I should see "([^"]*)" link$/) do |link|
  if page.find_link(link).visible?
    puts "#{link} link is visible"
  else
    fail(ArgumentError.new(link + ' link is not visible'))
  end
end

#step definition to click the particular link in a web page that specified in an feature step
When(/^I follow "([^"]*)" link$/) do |link|
  find_link(link).visible?
  click_link(link)
end

#step definition to wait for time we mention in a method
When(/^I wait for (\d+) seconds$/) do |seconds|
  sleep(seconds.to_i)
end

#step definition to click on button under top button row
When(/^I click on "([^"]*)" button on top button row$/) do |button_name|
  sleep 5
  within all('.pbButton')[0] do
    click_on button_name
  end
end


#Step to click on new button
When(/^I follow on "([^"]*)" button$/) do |button_name|
  if find_button(button_name).visible?
    click_on button_name
  else
    writeFailure "#{button_name} button is not visible"
  end
  sleep 5
end


#step definition to click on edit,save,cancel Button in the opened Quotes page
When(/^I click on "([^"]*)" button from top row$/) do |val|
  sleep 2
  within(:id,'topButtonRow') do
    if find(:button, val).visible?
      click_on val
    end
  end
  sleep 2
  if page.has_content?("Error: Invalid Data. ")
    fail(ArgumentError.new('Not'+val+ 'd successfully due to Invalid Data. '))
  else
    puts val + "d successfully"
  end
  sleep 2
end


Then(/^I should see the following:$/) do |text|
  page.should have_content(text)
end
