

Then(/^I should see Billing Frequency picklist values in new opportunity page layout$/) do
  if page.has_field?("Billing Frequency")
    actualvalues = find_field("Billing Frequency", match: :prefer_exact).all('option').collect(&:text)
    puts "actual values = #{actualvalues}"
    expectedvalues = ["--None--","In Advance","Annual","Quarterly","Monthly","In Arrears - Monthly","In Arrears - Quarterly","In Arrears - Annually"]
    puts "expectedvalues =  #{expectedvalues}"
    if actualvalues.sort == expectedvalues.sort
      puts "expected values are visible in Billing Frequency picklist"
    else
      Extravalue = (actualvalues.sort-expectedvalues.sort)
      puts  "Extra value = #{Extravalue}"
      MissingValue = (expectedvalues.sort - actualvalues.sort)
      puts  "Missing Value = #{MissingValue}"
      writeFailure "expected values are not visible in Billing Frequency picklist"
    end
  else
    writeFailure "Billing Frequency field not visible"
  end
end

When(/^I enter all mandantory fields to create new random opportunity$/) do
  if (ENV['UserRole'] == "OperationManagerSit") || (ENV['UserRole'] == "OperationRepSit") || (ENV['UserRole'] == "OperationManagerProd") || (ENV['UserRole'] == "OperationRepProd") || (ENV['UserRole'] == "AdminSit")
    randomvalue=('0000'..'9999').to_a.sample
    $newopportunity = "smoke-test-opportunity-" + randomvalue
    sleep 5
    puts $newopportunity
    fill_in('Opportunity Name', with: $newopportunity, match: :prefer_exact)
    fill_in('Account Name', with: "smoke-test-account-123", match: :prefer_exact)
    select('Not Contacted', :from => 'Stage', match: :prefer_exact)
    fill_in('Price Book', with: "Standard Price Book", match: :prefer_exact)
    fill_in('Close Date', with: " 7/21/2017", match: :prefer_exact)
    select('Volume', :from => 'Opportunity Type', match: :prefer_exact)
    select('Direct', :from => 'Direct/Channel', match: :prefer_exact)
    select('EMEA', :from => 'Client Territory', match: :prefer_exact)
    select('Turkey', :from => 'Client Region', match: :prefer_exact)
#    select('West', :from => 'Client Sub-Region', match: :prefer_exact)
    select('United States', :from => 'Country', match: :prefer_exact)
    select('NALA', :from => 'SSI Theatre', match: :prefer_exact)
    sleep 15
  else
    randomvalue=('0000'..'9999').to_a.sample
    $newopportunity = "smoke-test-opportunity-" + randomvalue
    sleep 5
    fill_in('Opportunity Name', with: $newopportunity, match: :prefer_exact)
    fill_in('Account Name', with: "smoke-test-account-123", match: :prefer_exact)
    select('Not Contacted', :from => 'Stage', match: :prefer_exact)
    fill_in('Price Book', with: "Standard Price Book", match: :prefer_exact)
    fill_in('Close Date', with: " 7/21/2017", match: :prefer_exact)
    select('Volume', :from => 'Opportunity Type', match: :prefer_exact)
    select('Direct', :from => 'Direct/Channel', match: :prefer_exact)
    select('EMEA', :from => 'Client Territory', match: :prefer_exact)
    select('RNA', :from => 'Client Region', match: :prefer_exact)
    select('United States', :from => 'Country', match: :prefer_exact)
    select('NALA', :from => 'SSI Theatre', match: :prefer_exact)
    sleep 15
  end
end